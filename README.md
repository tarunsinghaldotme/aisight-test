# AI Sight
## _Assessment test for DevOps_

## Task 1:
```
Directory flask-app: contains a flask application and its dockerfile
also gitlab pipeline has job for building it and pushing it to the ecr.
```

## Task 2:
```
Directory aisight-iac: containes cdk code for bootstrapping couple of lambda functions 
which are integrated with api gateway. also ecs cluster to launch above flask-app.
```

## Task 3:
```
Directory wordpress: contains docker copmpose for the wordpress site.
```
## Task 4:
```
Directory aisight-iac: I have combined this task of creating cloudwatch dashboard with the task 2. 
task 2 has the code to monitor both lambda function and both api gateway.
```

## Task 5:
```
Directory diagram: Has the diagram of the stack in png foramt.
```

## Task 6: 
`Pending...`


# Deliverables:

| URL | https://gitlab.com/tarunsinghaldotme/aisight-test |
| ------ | ------ |
| CICD | https://gitlab.com/tarunsinghaldotme/aisight-test/-/blob/main/.gitlab-ci.yaml |
| Api GW 1 | https://i5uzxwmxk5.execute-api.us-east-1.amazonaws.com|
| Api GW 2 | https://btnyo4bo9c.execute-api.us-east-1.amazonaws.com |
| ECS Flask Enpoint | http://aisig-flask-tz8z5uityn6z-2038175085.us-east-1.elb.amazonaws.com |
| CloudWatch Dashboard | https://us-east-1.console.aws.amazon.com/cloudwatch/home?region=us-east-1#dashboards:name=AISight-Dashboard |
