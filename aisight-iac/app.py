#!/usr/bin/env python3

from aws_cdk import core

from aisight_iac.aisight_iac_tasks_2 import FullStack


app = core.App()
FullStack(app, "aisight-iac")

app.synth()
