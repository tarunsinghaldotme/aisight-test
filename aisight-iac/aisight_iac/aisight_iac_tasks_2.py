from aws_cdk import (
    aws_iam as iam,
    aws_ec2 as ec2,
    aws_ecs as ecs,
    aws_ecs_patterns as ecs_patterns,
    aws_lambda,
    aws_apigatewayv2 as api_gw,
    aws_cloudwatch as cloudwatch,
    aws_apigatewayv2_integrations as integrations,
    core
)
import jsii


class FullStack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)
    
        project_name = self.node.try_get_context("project_name")
        environment =  self.node.try_get_context("env")

        full_name = f"{project_name}-{environment}"

        vpc = ec2.Vpc(self, full_name,
            cidr="10.8.0.0/16",
            max_azs=2,
            enable_dns_hostnames=True,
            enable_dns_support=True,
            subnet_configuration=[
                ec2.SubnetConfiguration(
                    name=f"{full_name}-public",
                    subnet_type=ec2.SubnetType.PUBLIC,
                    cidr_mask=24
                ),
                ec2.SubnetConfiguration(
                    name=f"{full_name}-private",
                    subnet_type=ec2.SubnetType.PRIVATE,
                    cidr_mask=24
                )
            ],
            nat_gateways=2
        )

        ecs_cluster = ecs.Cluster(self, f"{full_name}-cluster", vpc=vpc)
        flask_app_image = ecs.ContainerImage.from_asset("../flask-app")

        role = iam.Role(self, "FargateContainerRole", assumed_by=iam.ServicePrincipal("ecs-tasks.amazonaws.com"))

        flask_app_task_definition = ecs.FargateTaskDefinition(
            self, "FargateContainerTaskDefinition", execution_role=role, task_role=role, cpu=1024, memory_limit_mib=2048
        )

        flask_port_mapping = ecs.PortMapping(container_port=5000, host_port=5000)
        flask_container = flask_app_task_definition.add_container(
            "Container", image=flask_app_image,
            logging=ecs.AwsLogDriver(stream_prefix="flask-app")
        )
        flask_container.add_port_mappings(flask_port_mapping)

        flask_service = ecs_patterns.ApplicationLoadBalancedFargateService(self, "flask-app",
            cluster=ecs_cluster,
            task_definition=flask_app_task_definition,
            desired_count=2,
            public_load_balancer=True)

        
        # Setup AutoScaling policy
        scaling = flask_service.service.auto_scale_task_count(
            max_capacity=2
        )
        scaling.scale_on_cpu_utilization(
            "CpuScaling",
            target_utilization_percent=50,
            scale_in_cooldown=core.Duration.seconds(60),
            scale_out_cooldown=core.Duration.seconds(60),
        )

        lambda1 = aws_lambda.Function(self, "Lambda1",
                                                          handler='lambda_function.lambda_handler',
                                                          runtime=aws_lambda.Runtime.PYTHON_3_8,
                                                          code=aws_lambda.Code.from_asset(
                                                              '../lambda_functions/lambda1'),
                                                          vpc=vpc
                                                          )
        lambda2 = aws_lambda.Function(self, "Lambda2",
                                                          handler='lambda_function.lambda_handler',
                                                          runtime=aws_lambda.Runtime.PYTHON_3_8,
                                                          code=aws_lambda.Code.from_asset(
                                                              '../lambda_functions/lambda2'),
                                                          vpc=vpc
                                                          )


        api1 = api_gw.HttpApi(self, 'HttpAPI1',
                             default_integration=integrations.LambdaProxyIntegration(handler=lambda1))

        core.CfnOutput(self, 'HTTP API 1 Url', value=api1.url)

        api2 = api_gw.HttpApi(self, 'HttpAPI2',
                             default_integration=integrations.LambdaProxyIntegration(handler=lambda2))

        core.CfnOutput(self, 'HTTP API 2 Url', value=api2.url)


        cw_dashboard = cloudwatch.Dashboard(self, "Stack Monitoring Dashboard", dashboard_name="AISight-Dashboard")

        title_widget_lambda1 = cloudwatch.TextWidget(
            markdown="# Dashboard: {}".format(lambda1.function_name),
            height=1,
            width=24
        )

        title_widget_lambda2 = cloudwatch.TextWidget(
            markdown="# Dashboard: {}".format(lambda2.function_name),
            height=1,
            width=24
        )

        invocations_widget_lambda1 = cloudwatch.GraphWidget(title= "Invocations",
            left=[lambda1.metric_invocations()],
            width=24
        )

        invocations_widget_lambda2 = cloudwatch.GraphWidget(title= "Invocations",
            left=[lambda2.metric_invocations()],
            width=24
        )

        errors_widget_lambda1 = cloudwatch.GraphWidget(title= "Errors",
            left=[lambda1.metric_errors()],
            width=24
        )

        errors_widget_lambda2 = cloudwatch.GraphWidget(title= "Errors",
            left=[lambda2.metric_errors()],
            width=24
        )

        duration_widget_lambda1 = cloudwatch.GraphWidget(title= "Duration",
            left=[lambda1.metric_duration()],
            width=24
        )

        duration_widget_lambda2 = cloudwatch.GraphWidget(title= "Duration",
            left=[lambda2.metric_duration()],
            width=24
        )

        throttles_widget_lambda1 = cloudwatch.GraphWidget(title= "Throttles",
            left=[lambda1.metric_throttles()],
            width=24
        )

        throttles_widget_lambda2 = cloudwatch.GraphWidget(title= "Throttles",
            left=[lambda2.metric_throttles()],
            width=24
        )

        log_widget_lambda1 = cloudwatch.LogQueryWidget(log_group_names=[lambda1.log_group.log_group_name],
            query_lines=["fields @timestamp, @message", "sort @timestamp desc", "limit 20"], width=24)

        log_widget_lambda2 = cloudwatch.LogQueryWidget(log_group_names=[lambda2.log_group.log_group_name],
            query_lines=["fields @timestamp, @message", "sort @timestamp desc", "limit 20"], width=24)

        title_widget_api1 = cloudwatch.TextWidget(
            markdown="# Dashboard Api Gw 1: {}".format(api1.http_api_id),
            height=1,
            width=24
        )

        apigw1_widget_count = cloudwatch.GraphWidget(title="Requests",
                                                      width=8,
                                                      left=[self.metric_for_api_gw(api_id=api1.http_api_id,
                                                                                   metric_name="Count",
                                                                                   label="# Requests",
                                                                                   stat="sum")])
        apigw1_widget_latency = cloudwatch.GraphWidget(title="API GW Latency",
                                                      width=8,
                                                      stacked=True,
                                                      left=[self.metric_for_api_gw(api_id=api1.http_api_id,
                                                                                   metric_name="Latency",
                                                                                   label="API Latency p50",
                                                                                   stat="p50"),
                                                            self.metric_for_api_gw(api_id=api1.http_api_id,
                                                                                   metric_name="Latency",
                                                                                   label="API Latency p90",
                                                                                   stat="p90"),
                                                            self.metric_for_api_gw(api_id=api1.http_api_id,
                                                                                   metric_name="Latency",
                                                                                   label="API Latency p99",
                                                                                   stat="p99")
                                                            ])
        apigw1_widget_error = cloudwatch.GraphWidget(title="API GW Errors",
                                                      width=8,
                                                      stacked=True,
                                                      left=[self.metric_for_api_gw(api_id=api1.http_api_id,
                                                                                   metric_name="4XXError",
                                                                                   label="4XX Errors",
                                                                                   stat="sum"),
                                                            self.metric_for_api_gw(api_id=api1.http_api_id,
                                                                                   metric_name="5XXError",
                                                                                   label="5XX Errors",
                                                                                   stat="sum")
                                                            ])
                                                                                
        title_widget_api2 = cloudwatch.TextWidget(
            markdown="# Dashboard Api Gw 2: {}".format(api2.http_api_id),
            height=1,
            width=24
        )

        apigw2_widget_count = cloudwatch.GraphWidget(title="Requests",
                                                      width=8,
                                                      left=[self.metric_for_api_gw(api_id=api2.http_api_id,
                                                                                   metric_name="Count",
                                                                                   label="# Requests",
                                                                                   stat="sum")])
        apigw2_widget_latency = cloudwatch.GraphWidget(title="API GW Latency",
                                                      width=8,
                                                      stacked=True,
                                                      left=[self.metric_for_api_gw(api_id=api2.http_api_id,
                                                                                   metric_name="Latency",
                                                                                   label="API Latency p50",
                                                                                   stat="p50"),
                                                            self.metric_for_api_gw(api_id=api2.http_api_id,
                                                                                   metric_name="Latency",
                                                                                   label="API Latency p90",
                                                                                   stat="p90"),
                                                            self.metric_for_api_gw(api_id=api2.http_api_id,
                                                                                   metric_name="Latency",
                                                                                   label="API Latency p99",
                                                                                   stat="p99")
                                                            ])
        apigw2_widget_error = cloudwatch.GraphWidget(title="API GW Errors",
                                                      width=8,
                                                      stacked=True,
                                                      left=[self.metric_for_api_gw(api_id=api2.http_api_id,
                                                                                   metric_name="4XXError",
                                                                                   label="4XX Errors",
                                                                                   stat="sum"),
                                                            self.metric_for_api_gw(api_id=api2.http_api_id,
                                                                                   metric_name="5XXError",
                                                                                   label="5XX Errors",
                                                                                   stat="sum")
                                                            ])


        cw_dashboard.add_widgets(title_widget_lambda1,
                                 invocations_widget_lambda1,
                                 errors_widget_lambda1,
                                 duration_widget_lambda1,
                                 throttles_widget_lambda1,
                                 log_widget_lambda1,
                                 title_widget_lambda2,
                                 invocations_widget_lambda2,
                                 errors_widget_lambda2,
                                 duration_widget_lambda2,
                                 throttles_widget_lambda2,
                                 log_widget_lambda2,
                                 title_widget_api1,
                                 apigw1_widget_count,
                                 apigw1_widget_latency,
                                 apigw1_widget_error,
                                 title_widget_api2,
                                 apigw2_widget_count,
                                 apigw2_widget_latency,
                                 apigw2_widget_error
        )

        cloudwatch_dasboard_url = 'https://{}.console.aws.amazon.com/cloudwatch/home?region={}#dashboards:name=AISight-Dashboard'.format(
            core.Aws.REGION,
            core.Aws.REGION,
        )



        core.CfnOutput(
            self, "LoadBalancerDNS",
            value=f"http://{flask_service.load_balancer.load_balancer_dns_name}"
        )


        core.CfnOutput(self,"DashboardOutput",
            value=cloudwatch_dasboard_url,
            description="URL of AI SIGHT CloudWatch Dashboard",
            export_name="CloudWatchDashboardURL")


    @jsii.implements(cloudwatch.IMetric)
    def metric_for_api_gw(self, api_id: str, metric_name: str, label: str, stat: str = 'avg'):
        return self.build_metric(metric_name, "AWS/ApiGateway", {"ApiId": api_id}, cloudwatch.Unit.COUNT, label, stat)

    @staticmethod
    def build_metric(metric_name: str, name_space: str, dimensions, unit: cloudwatch.Unit, label: str,
                    stat: str = 'avg', period: int = 900):
        return cloudwatch.Metric(metric_name=metric_name,
                                namespace=name_space,
                                dimensions=dimensions,
                                unit=unit,
                                label=label,
                                statistic=stat,
                                period=core.Duration.seconds(period))


