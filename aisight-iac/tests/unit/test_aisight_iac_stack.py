import json
import pytest

from aws_cdk import core
from aisight_iac.aisight_iac_stack import AisightIacStack


def get_template():
    app = core.App()
    AisightIacStack(app, "aisight-iac")
    return json.dumps(app.synth().get_stack("aisight-iac").template)


def test_sqs_queue_created():
    assert("AWS::SQS::Queue" in get_template())


def test_sns_topic_created():
    assert("AWS::SNS::Topic" in get_template())
